# Omikronic Repository Manager #

The main purpose of this project is to create a standarized layer to manage content in different repositories. It manages three main concepts: Repositories, Content Structures and Contents.

This repository manager could be used as backend for different applications: custom apps, document management,  collaboration, web content management, record management, etc.


## Repository ##
This is a content structure and content container. It is a way to separate content structures and contents in different containers. But there is also a repository hierarchy that allows you to share content structures (and also contents) between repositories. A repository inherits the parent repository structures and contents to be able to refer to them inside.


## Content Structure ##
As its own name tells you, it is a way to create a structure for the content (fields, translations, editing and presenting configuration). Content Structures allow two kind of relationships between them: inheritance and composition. When a Content Structure inherits from other one, it can override the field translations and configurations, it can add more field (but it can not remove fields). Content Structures have versioning, so you can move between different versions easily.

For people from Content Management this concept is similar to Content Type or Node Type.
For people from Document Management this concept is similar to Document Type or Metadata Set.
  

## Content ##
It is each instance of a content structure with the fields filled. As Content Structures, it has versioning.

For people from Content Management this concept is similar to Content Instance or Content Item.
For people from Document Management this concept is similar to Document.


## Features Overview ##
* Repositories:
    - Create
    - Update
    - Delete
    - Move
    - Clone
* Content Structures:
	- Create
	- Update
	- Delete
	- Move
	- Clone
	- Versioning
* Contents:
	- Create
	- Update
	- Delete
	- Move
	- Clone
	- Versioning


## Architecture ##
There are planned a wide variety of entrypoints for manage repositories, content structures and contents.

### Entrypoints ###
* API:
	- Command Dispatcher
	- CMIS
	- Git
	- JCR
	- REST
	- WebDAV
* UI:
	- Web with Progressive Enhancement
	- Angular2

### Backends ###
* For Content:
	- API:
		+ CMIS
		+ JCR
	- DB:
		+ JPA
		+ Mongo
		+ Redis
	- Search:
		+ Solr
		+ Lucene
* For binaries (images, documents, etc.):
	- Filesystem
	- FTP
	- Git
	- WebDAV

### Technologies used in this project ###
* Languages:
    - Java
    - JS
* Spring:
    - Boot
    - Data JPA
    - Hateoas
    - MVC
* Angular2
* REST
* Testing:
    - Cucumber-JVM
    - JUnit
* Lombok
* Git
* Solr
* CMIS
* JCR
* WebDAV
* Databases:
    - H2
    - MySQL